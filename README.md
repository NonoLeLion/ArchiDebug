# ArchiDebug

Tools that you need :
 - Python3

## Installation

For the installation just add the repo to your path
*ex : `PATH=/home/dialo/Documents/ArchiDebug:$PATH`*

Make an alias :
`alias archi="bash ArchiDebug.sh"`

*NB : To save changes add this two lines to your .bashrc (or some other init files)*

## Start

There is an exemple :
```
dialo@dialo:~/ArchiDebug$ bash main.sh
dialo@dialo:~/ArchiDebug$ Pls enter an URL : http://www.debug-pro.com/epita/prog/s4/pw/pw_01_malloc/index.html
```

*NB : The architecture appear in __RESULT/__ and you can get the data file in __DATA/__*
